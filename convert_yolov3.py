import xml.etree.ElementTree as ET
import os,shutil

data_path='data'
coco_path='data/coco'

def getAnnotation(index):
    cwd = os.getcwd()
    my_data_path = os.path.join(cwd, data_path)
    my_coco_path = os.path.join(cwd, coco_path)
    filename = os.path.join(my_data_path, 'annotations', index + '.xml')
    tree = ET.parse(filename)
    objs = tree.findall('object')
    size = tree.find('size')

    dw = 1./int(size.find('width').text)
    dh = 1./int(size.find('height').text)

    annolist=[]
    for ix, obj in enumerate(objs):
        bbox = obj.find('bndbox')
        name = obj.find('name').text

        xmin = int(bbox.find('xmin').text)
        ymin = int(bbox.find('ymin').text)
        w = int(bbox.find('xmax').text)-xmin
        h = int(bbox.find('ymax').text)-ymin

        x = xmin + w/2.0 #central point
        y = ymin + h/2.0

        x = x * dw
        w = w * dw
        y = y * dh
        h = h * dh

        if(name=='trafficcone'):
            annolist.append(' '.join([str(x)for x in[0,x,y,w,h]]))
        else:
            print(index)

    with open(os.path.join(my_coco_path,'labels','trainval', index + '.txt'),'a') as f:
        f.write('\n'.join(annolist))

if __name__ == '__main__':
    cwd = os.getcwd()
    my_data_path = os.path.join(cwd, data_path)
    my_coco_path = os.path.join(cwd, coco_path)
    trainval = os.path.join(my_data_path, 'ImageSets','Main', 'trainval.txt')
    dir_list=[]
    with open(trainval, 'r')as f:
        lines=[x.strip() for x in f.readlines()]
        for line in lines:
            getAnnotation(line)
            src_dir=os.path.join(my_data_path, 'samples',line + '.jpg')
            dst_dir=os.path.join(my_coco_path, 'images','trainval',line + '.jpg')
            shutil.copyfile(src_dir,dst_dir)

            dir_list.append(dst_dir)
    with open(os.path.join(my_coco_path,'trainval.txt'),'a')as f:
        f.write('\n'.join(dir_list))
